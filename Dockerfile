FROM python:3.10
WORKDIR /automated-test-for-104intersection
COPY . /test
RUN pip install -r requirements.txt
EXPOSE 80
ENV NOM Hugo
CMD ["python", "104.py"]