# Automated test for 104intersection
Hi Epitech students, I'm offering you a series of tests to complement Marvin's to check if your program is working.
Please feel free to complete our battery of tests, so that it is beneficial to all.

## How to join ?
- [ ] [ssh-keygen -t rsa -b 2048 -C "your mail"] (Creation of a ssh key for GitLab)
- [ ] [ssh-keygen -t ed25519 -C "your mail"]
- [ ] [...]
- [ ] [sudo dnf install xclip] (Copy your ssh key in gitlab)
- [ ] [ssh -T git@gitlab.com] (To check the correct connection with GitLab)
- [ ] [git clone git@gitlab.com:TMGOD/automated-test-for-104intersection.git]

## How to test your programme ?
- [ ] [After git clones the repository]
- [ ] [First, delete the original file from the repository and replace it with the one you need to test. Do not delete the "tests.py" file !]
- [ ] [Then, you can run "tests.py" or test on your GitLab account to automate the tests.]

## How to contribute ?
- [ ] [To help expand our tests, you can make a pull request or send me on Discord the tests.]
- [ ] [The tests will be formatted like this:]
- [ ] [./binary "arg" "arg" "arg"] -> [return "expected return value"] -> [display "expected output in the terminal"]

    Example : ./104intersection -e -> return 84 -> None

## Python version
- [ ] [The version of python has been set to 3.10]

## Disclaimer
- [ ] [Epitech student, beware of -42.]
- [ ] [The aim is not to copy the test code. Its only purpose is to test the output of your program.]
- [ ] [It is not a unit test.]

### Contributor

    {Hugo Payet}