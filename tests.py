#!/usr/bin/env -S python3
import unittest
import sys
from io import StringIO
from unittest.mock import patch

intersection = __import__('104intersection')

first_example = """Sphere of radius 1
Line passing through the point (0, 0, 2) and parallel to the vector (1, 1, 0)
No intersection point.\n"""

second_example = """Sphere of radius 4
Line passing through the point (4, 0, 3) and parallel to the vector (0, 0, -2)
1 intersection point:
(4.000, 0.000, 0.000)\n"""

third_example = """Cylinder of radius 1
Line passing through the point (0, 0, 2) and parallel to the vector (1, 1, 0)
2 intersection points:
(0.707, 0.707, 2.000)
(-0.707, -0.707, 2.000)\n"""

fourth_example = """Cone with a 30 degree angle
Line passing through the point (-1, -1, -1) and parallel to the vector (1, 1, 5)
2 intersection points:
(-1.568, -1.568, -3.842)
(-0.537, -0.537, 1.315)\n"""

fifth_example = """Cylinder of radius 1
Line passing through the point (1, 0, 0) and parallel to the vector (0, 0, 1)
There is an infinite number of intersection points.\n"""

display_help = """USAGE
    ./104intersection opt xp yp zp xv yv zv p

DESCRIPTION
    opt             surface option: 1 for a sphere, 2 for a cylinder, 3 for a cone
    (xp, yp, zp)    coordinates of a point by which the light ray passes through
    (xv, yv, zv)    coordinates of a vector parallel to the light ray
    p               parameter: radius of the sphere, radius of the cylinder, or
                    angle formed by the cone and the Z-axis\n"""


class Test_output_help(unittest.TestCase):
    @patch('sys.argv', ['104intersection', '-h'])
    def test_help(self):
        try:
            out = StringIO()
            sys.stdout = out
            intersection.main()
            output = out.getvalue()
            self.assertEqual(output, display_help)
        except SystemExit:
            output = out.getvalue()
            with self.assertRaises(SystemExit):
                intersection.main()
            self.assertEqual(output, display_help)

class Test_return_of_programme_for_help(unittest.TestCase):
    @patch('sys.argv', ['104intersection', '-h'])
    def test_help_return_value(self):
        try:
            self.assertEqual(intersection.main(), 0)
        except SystemExit:
            with self.assertRaises(SystemExit) as cm:
                intersection.main()
            self.assertEqual(cm.exception.code, 0)


class Test_bad_output_first_example(unittest.TestCase):
    @patch('sys.argv', ['104intersection', '1', '0', '0', '2', '1', '1', '0', '1'])
    def test_first_example_pdf(self):
        try:
            out = StringIO()
            sys.stdout = out
            intersection.main()
            output = out.getvalue()
            self.assertEqual(output, first_example)
        except SystemExit:
            output = out.getvalue()
            with self.assertRaises(SystemExit):
                intersection.main()
            self.assertEqual(output, first_example)

class Test_return_of_programme_first_example(unittest.TestCase):
    @patch('sys.argv', ['104intersection', '1', '0', '0', '2', '1', '1', '0', '1'])
    def test_first_example_pdf_value(self):
        try:
            self.assertEqual(intersection.main(), 0)
        except SystemExit:
            with self.assertRaises(SystemExit) as cm:
                intersection.main()
            self.assertEqual(cm.exception.code, 0)


class Test_output_second_example(unittest.TestCase):
    @patch('sys.argv', ['104intersection', '1', '4', '0', '3', '0', '0', '-2', '4'])
    def test_second_example(self):
        try:
            out = StringIO()
            sys.stdout = out
            intersection.main()
            output = out.getvalue()
            self.assertEqual(output, second_example)
        except SystemExit:
            output = out.getvalue()
            with self.assertRaises(SystemExit):
                intersection.main()
            self.assertEqual(output, second_example)

class Test_return_second_example(unittest.TestCase):
    @patch('sys.argv', ['104intersection', '1', '4', '0', '3', '0', '0', '-2', '4'])
    def test_second_example_pdf_value(self):
        try:
            self.assertEqual(intersection.main(), 0)
        except SystemExit:
            with self.assertRaises(SystemExit) as cm:
                intersection.main()
            self.assertEqual(cm.exception.code, 0)


class Test_output_third_example(unittest.TestCase):
    @patch('sys.argv', ['104intersection', '2', '0', '0', '2', '1', '1', '0', '1'])
    def test_third_example(self):
        try:
            out = StringIO()
            sys.stdout = out
            intersection.main()
            output = out.getvalue()
            self.assertEqual(output, third_example)
        except SystemExit:
            output = out.getvalue()
            with self.assertRaises(SystemExit):
                intersection.main()
            self.assertEqual(output, third_example)

class Test_return_third_example(unittest.TestCase):
    @patch('sys.argv', ['104intersection', '2', '0', '0', '2', '1', '1', '0', '1'])
    def test_third_example_pdf_value(self):
        try:
            self.assertEqual(intersection.main(), 0)
        except SystemExit:
            with self.assertRaises(SystemExit) as cm:
                intersection.main()
            self.assertEqual(cm.exception.code, 0)


class Test_output_fourth_example(unittest.TestCase):
    @patch('sys.argv', ['104intersection', '3', '-1', '-1', '-1', '1', '1', '5', '30'])
    def test_fourth_example(self):
        try:
            out = StringIO()
            sys.stdout = out
            intersection.main()
            output = out.getvalue()
            self.assertEqual(output, fourth_example)
        except SystemExit:
            output = out.getvalue()
            with self.assertRaises(SystemExit):
                intersection.main()
            self.assertEqual(output, fourth_example)

class Test_return_fourth_example(unittest.TestCase):
    @patch('sys.argv', ['104intersection', '3', '-1', '-1', '-1', '1', '1', '5', '30'])
    def test_fourth_example_pdf_value(self):
        try:
            self.assertEqual(intersection.main(), 0)
        except SystemExit:
            with self.assertRaises(SystemExit) as cm:
                intersection.main()
            self.assertEqual(cm.exception.code, 0)


class Test_output_fifth_example(unittest.TestCase):
    @patch('sys.argv', ['104intersection', '2', '1', '0', '0', '0', '0', '1', '1'])
    def test_fifth_example(self):
        try:
            out = StringIO()
            sys.stdout = out
            intersection.main()
            output = out.getvalue()
            self.assertEqual(output, fifth_example)
        except SystemExit:
            output = out.getvalue()
            with self.assertRaises(SystemExit):
                intersection.main()
            self.assertEqual(output, fifth_example)

class Test_return_fifth_example(unittest.TestCase):
    @patch('sys.argv', ['104intersection', '2', '1', '0', '0', '0', '0', '1', '1'])
    def test_fifth_example_pdf_value(self):
        try:
            self.assertEqual(intersection.main(), 0)
        except SystemExit:
            with self.assertRaises(SystemExit) as cm:
                intersection.main()
            self.assertEqual(cm.exception.code, 0)


class Test_return_no_arg(unittest.TestCase):
    @patch('sys.argv', ['104intersection'])
    def test_return_no_arg(self):
        try:
            self.assertEqual(intersection.main(), 84)
        except SystemExit:
            with self.assertRaises(SystemExit) as cm:
                intersection.main()
            self.assertEqual(cm.exception.code, 84)

class Test_return_not_enough_arg(unittest.TestCase):
    @patch('sys.argv', ['104intersection', '1', '0', '0', '2', '1', '1'])
    def test_return_not_enough_arg_v1(self):
        try:
            self.assertEqual(intersection.main(), 84)
        except SystemExit:
            with self.assertRaises(SystemExit) as cm:
                intersection.main()
            self.assertEqual(cm.exception.code, 84)

    @patch('sys.argv', ['104intersection', '1', '0', '0'])
    def test_return_not_enough_arg_v2(self):
        try:
            self.assertEqual(intersection.main(), 84)
        except SystemExit:
            with self.assertRaises(SystemExit) as cm:
                intersection.main()
            self.assertEqual(cm.exception.code, 84)

class Test_return_too_many_arg(unittest.TestCase):
    @patch('sys.argv', ['104intersection', '1', '0', '0', '2', '1', '1', '0', '1', '5'])
    def test_return_too_many_arg_v1(self):
        try:
            self.assertEqual(intersection.main(), 84)
        except SystemExit:
            with self.assertRaises(SystemExit) as cm:
                intersection.main()
            self.assertEqual(cm.exception.code, 84)

    @patch('sys.argv', ['104intersection', '1', '0', '0', '2', '1', '1', '0', '1', '2', '0'])
    def test_return_too_many_arg_v2(self):
        try:
            self.assertEqual(intersection.main(), 84)
        except SystemExit:
            with self.assertRaises(SystemExit) as cm:
                intersection.main()
            self.assertEqual(cm.exception.code, 84)


class Test_argv_is_not_digit_v0(unittest.TestCase):
    @patch('sys.argv', ['104intersection', 'a', '0', '0', '2', '1', '1', '0', '1'])
    def test_bad_argv_v0(self):
        try:
            self.assertEqual(intersection.main(), 84)
        except SystemExit:
            with self.assertRaises(SystemExit) as cm:
                intersection.main()
            self.assertEqual(cm.exception.code, 84)

    @patch('sys.argv', ['104intersection', '1', 'b', '0', '2', '1', '1', '0', '1'])
    def test_bad_argv_v1(self):
        try:
            self.assertEqual(intersection.main(), 84)
        except SystemExit:
            with self.assertRaises(SystemExit) as cm:
                intersection.main()
            self.assertEqual(cm.exception.code, 84)

    @patch('sys.argv', ['104intersection', '1', '0', 'c', '2', '1', '1', '0', '1'])
    def test_bad_argv_v2(self):
        try:
            self.assertEqual(intersection.main(), 84)
        except SystemExit:
            with self.assertRaises(SystemExit) as cm:
                intersection.main()
            self.assertEqual(cm.exception.code, 84)

    @patch('sys.argv', ['104intersection', '1', '0', '0', 'd', '1', '1', '0', '1'])
    def test_bad_argv_v3(self):
        try:
            self.assertEqual(intersection.main(), 84)
        except SystemExit:
            with self.assertRaises(SystemExit) as cm:
                intersection.main()
            self.assertEqual(cm.exception.code, 84)

    @patch('sys.argv', ['104intersection', '1', '0', '0', '2', 'e', '1', '0', '1'])
    def test_bad_argv_v4(self):
        try:
            self.assertEqual(intersection.main(), 84)
        except SystemExit:
            with self.assertRaises(SystemExit) as cm:
                intersection.main()
            self.assertEqual(cm.exception.code, 84)

    @patch('sys.argv', ['104intersection', '1', '0', '0', '2', '1', 'f', '0', '1'])
    def test_bad_argv_v5(self):
        try:
            self.assertEqual(intersection.main(), 84)
        except SystemExit:
            with self.assertRaises(SystemExit) as cm:
                intersection.main()
            self.assertEqual(cm.exception.code, 84)

    @patch('sys.argv', ['104intersection', '1', '0', '0', '2', '1', '1', 'g', '1'])
    def test_bad_argv_v6(self):
        try:
            self.assertEqual(intersection.main(), 84)
        except SystemExit:
            with self.assertRaises(SystemExit) as cm:
                intersection.main()
            self.assertEqual(cm.exception.code, 84)

    @patch('sys.argv', ['104intersection', '1', '0', '0', '2', '1', '1', '0', 'h'])
    def test_bad_argv_v7(self):
        try:
            self.assertEqual(intersection.main(), 84)
        except SystemExit:
            with self.assertRaises(SystemExit) as cm:
                intersection.main()
            self.assertEqual(cm.exception.code, 84)

class Test_argv_is_not_digit_v1(unittest.TestCase):
    @patch('sys.argv', ['104intersection', '"', '0', '0', '2', '1', '1', '0', '1'])
    def test_bad_argv_v1_0(self):
        try:
            self.assertEqual(intersection.main(), 84)
        except SystemExit:
            with self.assertRaises(SystemExit) as cm:
                intersection.main()
            self.assertEqual(cm.exception.code, 84)

    @patch('sys.argv', ['104intersection', '1', 'à', '0', '2', '1', '1', '0', '1'])
    def test_bad_argv_v1_1(self):
        try:
            self.assertEqual(intersection.main(), 84)
        except SystemExit:
            with self.assertRaises(SystemExit) as cm:
                intersection.main()
            self.assertEqual(cm.exception.code, 84)

    @patch('sys.argv', ['104intersection', '1', '0', '&', '2', '1', '1', '0', '1'])
    def test_bad_argv_v1_2(self):
        try:
            self.assertEqual(intersection.main(), 84)
        except SystemExit:
            with self.assertRaises(SystemExit) as cm:
                intersection.main()
            self.assertEqual(cm.exception.code, 84)

    @patch('sys.argv', ['104intersection', '1', '0', '0', ')', '1', '1', '0', '1'])
    def test_bad_argv_v1_3(self):
        try:
            self.assertEqual(intersection.main(), 84)
        except SystemExit:
            with self.assertRaises(SystemExit) as cm:
                intersection.main()
            self.assertEqual(cm.exception.code, 84)

    @patch('sys.argv', ['104intersection', '1', '0', '0', '2', '_', '1', '0', '1'])
    def test_bad_argv_v1_4(self):
        try:
            self.assertEqual(intersection.main(), 84)
        except SystemExit:
            with self.assertRaises(SystemExit) as cm:
                intersection.main()
            self.assertEqual(cm.exception.code, 84)

    @patch('sys.argv', ['104intersection', '1', '0', '0', '2', '1', '!', '0', '1'])
    def test_bad_argv_v1_5(self):
        try:
            self.assertEqual(intersection.main(), 84)
        except SystemExit:
            with self.assertRaises(SystemExit) as cm:
                intersection.main()
            self.assertEqual(cm.exception.code, 84)

    @patch('sys.argv', ['104intersection', '1', '0', '0', '2', '1', '1', '<', '1'])
    def test_bad_argv_v1_6(self):
        try:
            self.assertEqual(intersection.main(), 84)
        except SystemExit:
            with self.assertRaises(SystemExit) as cm:
                intersection.main()
            self.assertEqual(cm.exception.code, 84)

    @patch('sys.argv', ['104intersection', '1', '0', '0', '2', '1', '1', '0', '='])
    def test_bad_argv_v1_7(self):
        try:
            self.assertEqual(intersection.main(), 84)
        except SystemExit:
            with self.assertRaises(SystemExit) as cm:
                intersection.main()
            self.assertEqual(cm.exception.code, 84)

class Test_bad_option(unittest.TestCase):
    @patch('sys.argv', ['104intersection', '0', '1', '0', '0', '0', '0', '1', '1'])    
    def test_bad_output_v0(self):
        try:
            self.assertEqual(intersection.main(), 84)
        except:
            with self.assertRaises(SystemExit) as cm:
                intersection.main()
            self.assertEqual(cm.exception.code, 84)
    
    @patch('sys.argv', ['104intersection', '1', '1', '0', '0', '0', '0', '1', '1'])    
    def test_bad_output_v1(self):
        try:
            self.assertEqual(intersection.main(), 0)
        except:
            with self.assertRaises(SystemExit) as cm:
                intersection.main()
            self.assertEqual(cm.exception.code, 0)

    @patch('sys.argv', ['104intersection', '2', '1', '0', '0', '0', '0', '1', '1'])    
    def test_bad_output_v2(self):
        try:
            self.assertEqual(intersection.main(), 0)
        except:
            with self.assertRaises(SystemExit) as cm:
                intersection.main()
            self.assertEqual(cm.exception.code, 0)

    @patch('sys.argv', ['104intersection', '3', '1', '0', '0', '0', '0', '1', '1'])    
    def test_bad_output_v3(self):
        try:
            self.assertEqual(intersection.main(), 0)
        except:
            with self.assertRaises(SystemExit) as cm:
                intersection.main()
            self.assertEqual(cm.exception.code, 0)

    @patch('sys.argv', ['104intersection', '4', '1', '0', '0', '0', '0', '1', '1'])    
    def test_bad_output_v4(self):
        try:
            self.assertEqual(intersection.main(), 84)
        except:
            with self.assertRaises(SystemExit) as cm:
                intersection.main()
            self.assertEqual(cm.exception.code, 84)

    @patch('sys.argv', ['104intersection', '55', '1', '0', '0', '0', '0', '1', '1'])    
    def test_bad_output_v5(self):
        try:
            self.assertEqual(intersection.main(), 84)
        except:
            with self.assertRaises(SystemExit) as cm:
                intersection.main()
            self.assertEqual(cm.exception.code, 84)

    @patch('sys.argv', ['104intersection', '1042', '1', '0', '0', '0', '0', '1', '1'])    
    def test_bad_output_v6(self):
        try:
            self.assertEqual(intersection.main(), 84)
        except:
            with self.assertRaises(SystemExit) as cm:
                intersection.main()
            self.assertEqual(cm.exception.code, 84)

class Test_vector_null(unittest.TestCase):
    @patch('sys.argv', ['104intersection', '1', '0', '0', '0', '0', '0', '0', '1'])
    def test_vector_null_v0(self):
        try:
            self.assertEqual(intersection.main(), 84)
        except:
            with self.assertRaises(SystemExit) as cm:
                intersection.main()
            self.assertEqual(cm.exception.code, 84)

    @patch('sys.argv', ['104intersection', '1', '1', '1', '0', '0', '0', '0', '1'])
    def test_vector_null_v1(self):
        try:
            self.assertEqual(intersection.main(), 84)
        except:
            with self.assertRaises(SystemExit) as cm:
                intersection.main()
            self.assertEqual(cm.exception.code, 84)
    
    @patch('sys.argv', ['104intersection', '2', '0', '0', '0', '0', '0', '0', '1'])
    def test_vector_null_v2(self):
        try:
            self.assertEqual(intersection.main(), 84)
        except:
            with self.assertRaises(SystemExit) as cm:
                intersection.main()
            self.assertEqual(cm.exception.code, 84)

    @patch('sys.argv', ['104intersection', '2', '1', '1', '0', '0', '0', '0', '1'])
    def test_vector_null_v3(self):
        try:
            self.assertEqual(intersection.main(), 84)
        except:
            with self.assertRaises(SystemExit) as cm:
                intersection.main()
            self.assertEqual(cm.exception.code, 84)
    
    @patch('sys.argv', ['104intersection', '3', '0', '0', '0', '0', '0', '0', '1'])
    def test_vector_null_v4(self):
        try:
            self.assertEqual(intersection.main(), 84)
        except:
            with self.assertRaises(SystemExit) as cm:
                intersection.main()
            self.assertEqual(cm.exception.code, 84)

    @patch('sys.argv', ['104intersection', '3', '1', '1', '0', '0', '0', '0', '1'])
    def test_vector_null_v5(self):
        try:
            self.assertEqual(intersection.main(), 84)
        except:
            with self.assertRaises(SystemExit) as cm:
                intersection.main()
            self.assertEqual(cm.exception.code, 84)

class Test_bad_radius(unittest.TestCase):
    @patch('sys.argv', ['104intersection', '1', '0', '0', '2', '1', '1', '0', '-1'])
    def test_bad_radius_v0(self):
        try:
            self.assertEqual(intersection.main(), 84)
        except:
            with self.assertRaises(SystemExit) as cm:
                intersection.main()
            self.assertEqual(cm.exception.code, 84)

    @patch('sys.argv', ['104intersection', '2', '0', '0', '2', '1', '1', '0', '-1'])
    def test_bad_radius_v1(self):
        try:
            self.assertEqual(intersection.main(), 84)
        except:
            with self.assertRaises(SystemExit) as cm:
                intersection.main()
            self.assertEqual(cm.exception.code, 84)

    @patch('sys.argv', ['104intersection', '3', '-1', '-1', '-1', '1', '1', '5', '-1'])
    def test_bad_radius_v2(self):
        try:
            self.assertEqual(intersection.main(), 84)
        except:
            with self.assertRaises(SystemExit) as cm:
                intersection.main()
            self.assertEqual(cm.exception.code, 84)

class Test_bad_radius_v1(unittest.TestCase):
    @patch('sys.argv', ['104intersection', '1', '0', '0', '2', '1', '1', '0', '360'])
    def test_bad_radius_v1_0(self):
        try:
            self.assertEqual(intersection.main(), 84)
        except:
            with self.assertRaises(SystemExit) as cm:
                intersection.main()
            self.assertEqual(cm.exception.code, 84)

    @patch('sys.argv', ['104intersection', '2', '0', '0', '2', '1', '1', '0', '360'])
    def test_bad_radius_v1_1(self):
        try:
            self.assertEqual(intersection.main(), 84)
        except:
            with self.assertRaises(SystemExit) as cm:
                intersection.main()
            self.assertEqual(cm.exception.code, 84)

    @patch('sys.argv', ['104intersection', '3', '-1', '-1', '-1', '1', '1', '5', '90'])
    def test_bad_radius_v1_2(self):
        try:
            self.assertEqual(intersection.main(), 84)
        except:
            with self.assertRaises(SystemExit) as cm:
                intersection.main()
            self.assertEqual(cm.exception.code, 84)

if __name__ == '__main__':
    unittest.main()
